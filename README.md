# vaadin12-multiple-wars-in-one-ear

This project demonstrates packaging two Vaadin 12 CDI WARs inside a master EAR.

Although the EAR packaging format is often frowned upon due to its many pitfalls,
it is really helpful for building and distributing large modular applications
that consist of multiple WARs and EJB JARs.

Packaging multiple WARs into a master EAR **does currently not work in Vaadin 8**,
see the relevant issue in [Vaadin CDI bugtracker](https://github.com/vaadin/cdi/issues/97),
so it is great news that it works in Vaadin 12. All praise is due to
[Kimmel Tamás](https://vaadin.com/blog/kimmel-tamas-a-cdi-expert-and-vaadin-contributor)
who did most of the work for Vaadin 12 CDI.

## Instructions

It’s a Maven project with three modules:

* *webapp1* and *webapp2* – Vaadin 12 CDI projects created with the official
  starter, open <http://localhost:8080/webapp1> and <http://localhost:8080/webapp2>
  to access them
* *ear* – packages the WARs into an EAR.

Build the application with `mvn package` and deploy `ear/target/ear.ear` to the
application server to run it.

Tested to work with WildFly 14, other application servers may have problems, see
comments [in this GitHub issue](https://github.com/kumm/flow-cdi-addon/issues/4).

Here's how the result looks:

    ear.ear
    ├── webapp1.war
    └── webapp2.war

Here's a screenshot that demonstrates that there are no cross-WAR session
issues due to `@VaadinSessionScoped` or `@PWA` scope misuse:

![image](doc/screenshot.png)
